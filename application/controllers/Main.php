<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    public function index()
    {
        $this->session->sess_destroy();		//セッションデータの削除
        $this->welcome();
    }

    public function welcome(){
        $this->load->view('welcome');
    }

    public function login(){
        $this->load->view('login');
    }

    // ログアウト
    public function logout(){
        $this->session->sess_destroy();		//セッションデータの削除
        redirect ("main/login");		//ログインページにリダイレクトする
    }

    public function demo(){
        $this->load->view('demo');
    }

    // ログインボタン押下時
    public function login_validation(){

        $this->load->library("form_validation");	//フォームバリデーションライブラリを読み込む。
        //利用頻度の高いライブラリ（HTMLヘルパー、URLヘルパーなど）はオートロード設定をしますが、
        //フォームバリデーションライブラリはログインバリデーションライブラリ内のみで読み込みます。

        // email,pass必須 パスワードをDB登録時に暗号化
        $this->form_validation->set_rules("email", "メール", "required|trim");
        // パスワードチェック時に同時にvalidate_credentialsをコールバックする
        $this->form_validation->set_rules("password", "パスワード", "required|trim|callback_validate_credentials");

        if($this->form_validation->run()){		//バリデーションエラーがなかった場合の処理

            //自分のユーザーidをセッションにセットする
            if($id = $this->model_users->get_user_id($this->input->post("email"))){
            }else {echo "fail to add user. please try again";}
            
            //自分の名前をセッションにセットする
            if($presidentname = $this->model_users->get_user_presidentname($this->input->post("email"))){
            }else echo "fail to add user. please try again";

            $data = array(
                          "presidentname" => $presidentname,
                          "id" => $id,
                          "email" => $this->input->post("email"),
                          "is_logged_in" => 1
                          );
            $this->session->set_userdata($data);

            redirect("main/mypage");
        }else{						//バリデーションエラーがあった場合の処理
            //            $this->load->view("login");
            redirect ("main/login");		//ログインページにリダイレクトする
        }

    }

    public function validate_credentials(){		//Email情報がPOSTされたときに呼び出されるコールバック機能
        $this->load->model("model_users");

        if($this->model_users->can_log_in()){	//ユーザーがログインできたあとに実行する処理
            return true;
        }else{					//ユーザーがログインできなかったときに実行する処理
            $this->form_validation->set_message("validate_credentials", "ユーザー名かパスワードが異なります。");
            return false;
        }
    }

    // ログイン成功時、マイページに遷移する
    //public function members(){
    //    if($this->session->userdata("is_logged_in")){	//ログインしている場合の処理

    //        $email = $this->session->userdata("email");
    //        $data = array(
    //            "email" => $email
    //        );
    //        $this->load->view("member", $data);
    //    }else{									//ログインしていない場合の処理
    //         redirect ("main/restricted");
    //    }
    //}

    public function restricted(){
        $this->load->view("restricted");
    }

    public function signup(){
        $this->load->view("signup");
    }

    // 登録するボタン押下時
    public function complete(){
        $mode = $this->input->post("mode");

        if($mode == "登録"){

            // 登録が完了した場合、完了画面遷移する
            $this->load->model("model_users");
            //add_usersがTrueを返したら以下を実行
            if($newemail = $this->model_users->add_users()){
                
                //自分のユーザーidをセッションにセットする
                if($id = $this->model_users->get_user_id($newemail)){

                    $data = array(
                                  "presidentname" => $this->input->post("presidentname"),
                                  "id" => $id,
                                  "email" => $newemail,
                                  "is_logged_in" => 1
                                  );
                    
                } else {
                    echo "fail to add user. please try again.";
                };
                
                // 決算テーブルにデフォルトデータを入れる
                $this->model_users->insert_kessan_data_first($id);
                $this->session->set_userdata($data);

                // 資産状況テーブルにデフォルトデータを入れる
                $this->model_users->insert_funds_data_first($id);
                $this->session->set_userdata($data);


                $this->load->view("complete", $data);

            } else {
                echo "fail to add user. please try again";
            };

        } else {
            $data['email'] = $this->input->post("email");
            $this->load->view("signup", $data);
        };
    }

    //新規会員登録バリデーション
    public function signup_validation(){
        //フォームバリデーションのライブラリを読み込む
        $this->load->library("form_validation");
        // is_unique[DB.カラム名]でDB内で一意チェックしてくれる
        $this->form_validation->set_rules("presidentname", "お名前", "required|trim");
        $this->form_validation->set_rules("presidentnamekana", "フリガナ", "required|trim");
        $this->form_validation->set_rules("email", "メールアドレス", "required|trim|valid_email|is_unique[users.email]");
        $this->form_validation->set_rules("cemail", "メールアドレス確認用", "required|trim|valid_email|matches[email]");
        $this->form_validation->set_rules("password", "パスワード", "required|trim");
        $this->form_validation->set_rules("cpassword", "パスワード確認用", "required|trim|matches[password]");

        //メアドが被ってる時のエラーメッセージを設定
        $this->form_validation->set_message("is_unique", "入力したメールアドレスはすでに登録されています。");

        if($this->form_validation->run()){

            // バリデーションを通った場合、確認画面へ遷移する.

            // arrayに渡したい値を設定
            $data['presidentname'] = $this->input->post("presidentname");
            $data['presidentnamekana'] = $this->input->post("presidentnamekana");
            $data['email'] = $this->input->post("email");
            $data['cemail'] = $this->input->post("cemail");
            $data['password'] = $this->input->post("password");

            // パスワードの長さ文、●で表示する
            $passwordango = "";
            for ($i = 1; $i <= mb_strlen($this->input->post("password")); $i++) {
                $passwordango .= "●";
            }
            $data['passwordango'] = $passwordango;

            // view呼び出しの第二引数にarrayを設定
            $this->load->view("confirm", $data);

        }else{
            //echo "You can't pass,,,";
            $this->load->view("signup");
        }

    }

    // マイページに遷移する
    public function mypage(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

        
            $this->load->model("Model_users");
            
            //DBよりユーザー情報を取得する
            if($userdata = $this->Model_users->select_user_data()){
                $data = $userdata;
                
            }else {
                $data = array();
            }
            
            $this->load->view("mypage",$data);
        } else {

            redirect("main/login");
        }
    }

    // 編集画面に遷移する
    public function edit(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $mode = $this->input->post("mode");
        
            if($mode == "編集") {
                // ビューに値をセットする
                $data = array(
                              "company" => $this->input->post("company"),
                              "presidentname" => $this->input->post("presidentname"),
                              "birthdt" => $this->input->post("birthdt"),
                              "capitalstock" => $this->input->post("capitalstock"),
                              "targetamount" => $this->input->post("targetamount"),
                              "targetcontext" => $this->input->post("targetcontext"),
                              "filename" => $this->input->post("filename"),
                              "overFlg" => "0"
                              );
                $this->load->view("edit",$data);
            } else {
                redirect("main/mypage");
            }
        } else {
            redirect("main/login");
        }
    }
    
    public function yyymmdd_check($date)
    {
        if (preg_match("/^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
            return true;
        } else {
            $this->form_validation->set_message('yyymmdd_check', '正しい形式で入力してください。');
            return false;
        }
    }
    
    //編集時バリデーション
    public function edit_validation(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $mode = $this->input->post("mode");

            if($mode == "確認") {

                $this->load->model("model_users");

                //フォームバリデーションのライブラリを読み込む
                $this->load->library("form_validation");
                // TODO マイページ項目のバリデーションどうするか
                $this->form_validation->set_rules("birthdt", "設立日", "max_length[8]");
                $this->form_validation->set_rules("birthdt", "設立日", "callback_yyymmdd_check");


                if($this->form_validation->run()){

                    // 画像がアップロードされている場合
                    if (!empty($_FILES['userfile']['name'])) {
                    
                        $this->load->helper(array('form', 'url'));

                        // アップされた画像を一時的に指定したディレクトリに格納する
                        $config['upload_path']          = './uploads/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['max_size']             = 1000;
                        $config['max_width']            = 1000;
                        $config['max_height']           = 1000;

                        //var_dump($config);

                        $this->load->library('upload', $config);

                        if ( ! $this->upload->do_upload('userfile'))
                            {
                                $error = array('error' => $this->upload->display_errors());
                                //var_dump($error);
                                echo "画像がサイズが大きすぎます。1000×1000ピクセル以内の画像をアップロードしてください。";
                                $data = array(
                                              "filename" => $this->input->post("beforefilename"),
                                              "overFlg" => "1"
                                              );
                                $this->load->view("edit",$data);
                                return;
                            }
                        else
                            {
                                $filename =  $this->upload->data('file_name');
                                $dataImg[]= array(
                                                  // URLバージョンのギャラリーパスを利用
                                                  "url" =>"/uploads/" . $filename,
                                                  "thumb_url" =>"/uploads/".$filename
                                                  );
                            }
                    }else {
                        // 画像アップロードがなければ、以前の画像名を保持する
                        $filename =  $this->input->post("beforefilename");
                        echo $filename;
                        // すでに画像がアップされていた場合
                        if ($filename != ""){
                            $dataImg[]= array(
                                              // URLバージョンのギャラリーパスを利用
                                              "url" =>"/uploads/" . $filename,
                                              "thumb_url" =>"/uploads/".$filename
                                              );
                            // 画像がアップされておらず、新たに画像のアップも行わない場合
                        } else {
                            //echo 3;
                            $dataImg[]= array();
                        }
                    }

                    // バリデーションを通った場合、確認画面へ遷移する.
                    $data = array(
                                  "company" => $this->input->post("company"),
                                  "presidentname" => $this->input->post("presidentname"),
                                  "birthdt" => $this->input->post("birthdt"),
                                  "capitalstock" => $this->input->post("capitalstock"),
                                  "targetamount" => $this->input->post("targetamount"),
                                  "targetcontext" => $this->input->post("targetcontext"),
                                  "images" => $dataImg,
                                  "filename" => $filename,
                                  "beforefilename" => $this->input->post("beforefilename"),
                                  "overFlg" => "0"
                                  );

                    // view呼び出しの第二引数にarrayを設定
                    $this->load->view("edit_confirm", $data);

                }else{
                    $data = array(
                                  "filename" => $this->input->post("beforefilename"),
                                  "overFlg" => "0"
                                  );
                    $this->load->view("edit",$data);
                }
            } else {
                // マイページに戻る
                $this->mypage();
            }
        }else{
            redirect("main/login");
        }
    }

    // 登録するボタン押下時
    public function edit_confirm(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $mode = $this->input->post("mode");

            $data = array(
                          "company" => $this->input->post("company"),
                          "presidentname" => $this->input->post("presidentname"),
                          "birthdt" => $this->input->post("birthdt"),
                          "capitalstock" => $this->input->post("capitalstock"),
                          "targetamount" => $this->input->post("targetamount"),
                          "targetcontext" => $this->input->post("targetcontext"),
                          "pics" => $this->input->post("filename")
                          );

            if($mode == "更新"){

                // 登録が完了した場合、完了画面遷移する
                $this->load->model("model_users");
                //add_usersがTrueを返したら以下を実行
                if($this->model_users->update_userdata($data)){
                    $this->load->view("update_complete");
                }else {
                    echo "fail to add user. please try again";
                }
                
            } else {

                // 確認画面で戻る際は画像を以前の画像に戻す
                $data = array(
                              "company" => $this->input->post("company"),
                              "presidentname" => $this->input->post("presidentname"),
                              "birthdt" => $this->input->post("birthdt"),
                              "capitalstock" => $this->input->post("capitalstock"),
                              "targetamount" => $this->input->post("targetamount"),
                              "targetcontext" => $this->input->post("targetcontext"),
                              "filename" => $this->input->post("beforefilename"),
                              "overFlg" => "0"
                              );
                $this->load->view("edit", $data);
            }
        } else {
            redirect("main/login");
        }
    }

    // ユーザー一覧を表示する
    public function userlist(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $this->load->model("Model_users");

            //DBよりユーザー情報を取得する
            if($userdata = $this->Model_users->select_user_list()){
                $data = $userdata;
            }else {
                $data = array();
            }

            $this->load->view("members",$data);

        } else {
            redirect("main/login");
        }
    }

    // ユーザー詳細を表示する
    public function userDetail($id=NULL){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $this->load->model("model_users");

            //DBよりいいね数を取得する
            if($userdata = $this->model_users->select_user_datail($id)){
                $data = $userdata;
            }else echo "fail to add user. please try again";

            $this->load->view("detail",$data);

        }else{
            redirect("main/login");
        }
    }

    // いいねする
    public function good(){

        $mode = $this->input->post("mode");

        // いいねをする
        if($mode == "いいね"){

            $this->load->model("model_users");

            $id = $this->input->post("id");

            // いいねをする
            if($newemail = $this->model_users->add_good($id)){
                //DBより再度、いいね数を取得する
                if($userdata = $this->model_users->select_user_datail_after_good($id)){
                    $data = $userdata;
                }else echo "fail to add user. please try again";

                $this->load->view("detail",$data);
            }
        } else {
            // ユーザー一覧画面に戻る
            $this->userlist();
        }
    }

    // いいねされたユーザー一覧画面へ遷移
    public function liked(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){
        
            $this->load->model("model_users");

            //DBよりユーザー情報を取得する
            if($userdata = $this->model_users->select_good_user_list()){
                $data = $userdata;
            }else {

                echo "fail to add user. please try again";

            }

            $this->load->view("liked",$data);

        } else {
            redirect("main/login");
        }
    }

    // いいねしたユーザー一覧画面へ遷移
    public function like(){

        $this->load->helper('url');

        if(isset($_SESSION['email'])){

            $this->load->model("model_users");

            //DBよりユーザー情報を取得する
            if($userdata = $this->model_users->select_good_shita_user_list()){
                $data = $userdata;
            }else {

                echo "fail to add user. please try again";

            }

            $this->load->view("like",$data);

        }else{
            redirect("main/login");
        }
    }

    // 管理ページへ遷移する
    public function admin(){
        $this->session->sess_destroy();     //セッションデータの初期化
        $this->load->view('adminlogin');
    }

    // 管理ログインボタン押下時
    public function adminlogin_validation(){

        $this->load->library("form_validation");    //フォームバリデーションライブラリを読み込む。
        //利用頻度の高いライブラリ（HTMLヘルパー、URLヘルパーなど）はオートロード設定をしますが、
        //フォームバリデーションライブラリはログインバリデーションライブラリ内のみで読み込みます。

        // email,pass必須 パスワードをDB登録時に暗号化
        $this->form_validation->set_rules("userid", "ユーザーID", "required|trim");
        // パスワードチェック時に同時にvalidate_credentialsをコールバックする
        $this->form_validation->set_rules("password", "パスワード", "required|trim|callback_validate_admin_credentials");

        if($this->form_validation->run()){      //バリデーションエラーがなかった場合の処理

            $data = array(
                          "is_admin_logged_in" => 1
                          );
            $this->session->set_userdata($data);

            redirect("main/admin_members");
        }else{                      //バリデーションエラーがあった場合の処理
            //            $this->load->view("login");
            $this->load->view('adminlogin');
        }

    }

    //ユーザーが管理画面ログインボタン押下時に呼び出される
    public function validate_admin_credentials(){
        $this->load->model("model_users");

        if($this->model_users->can_admin_log_in()){ //ユーザーが管理画面ログインできたあとに実行する処理
            return true;
        }else{
            $this->form_validation->set_message("validate_credentials", "ユーザー名かパスワードが異なります。");
            return false;
        }
    }

    // 管理ログイン成功時、ユーザー一覧ページに遷移する
    public function admin_members(){
        if($this->session->userdata("is_admin_logged_in")){ //ログインしている場合の処理

            $this->load->model("model_users");

            //DBよりユーザー情報を取得する
            if($userdata = $this->model_users->select_user_list_for_admin()){
                $data = $userdata;
            }else {
                $data = array();
            }
            $this->load->view("members_for_admin",$data);
        }else{                                  //ログインしていない場合の処理
            redirect ("main/restricted");
        }
    }

    // ユーザー詳細を表示する
    public function userDetailForAdmin($id=NULL){

        $this->load->model("model_users");

        //DBよりいいね数を取得する
        if($userdata = $this->model_users->select_user_datail_for_admin($id)){
            $data = $userdata;
        }else echo "fail to add user. please try again";

        $this->load->view("detail_for_admin",$data);
    }

    // 登録するボタン押下時
    public function detail_admin_form(){

        $mode = $this->input->post("mode");
        $this->load->model("model_users");
        $id = $this->input->post("id");
        if($mode == "決算書をアップロードする"){
            $data = array(
                          "id" => $id
                          );
            $this->load->view("csv_upload",$data);
        }elseif($mode == "資産状況をアップロードする"){
            $data = array(
                          "id" => $id
                          );
            $this->load->view("csv_upload_funds",$data);
        }elseif($mode == "退会させる"){

            if($userdata = $this->model_users->select_user_datail_for_admin($id)){
                $data = $userdata;
            }else echo "fail to add user. please try again";
            $this->load->view("delete_confirm_for_admin",$data);
        } else {
            $this->admin_members();
        }
    }

    // 退会させるボタン押下時
    public function detail_admin_complete(){

        $mode = $this->input->post("mode");
        $id = $this->input->post("id");

        $this->load->model("model_users");
        if($mode == "退会させる"){

            // DBよりユーザー情報を削除する
            if($this->model_users->delete_user_data()){
            }else echo "fail to add user. please try again";

            $this->load->view("delete_complete");
        } else {
            // 戻る
            $this->userDetailForAdmin($id);
        }
    }

    //csvアップロード
    public function csv_upload(){

        $mode = $this->input->post("mode");
        $id = $this->input->post("id");
        if($mode == "アップロードする") {

            // csvがアップロードされている場合
            if (!empty($_FILES['userfile']['name'])) {

                $count=0;
                $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
                while($csv_line = fgetcsv($fp,1024))
                    {
                        $count++;
                        if($count == 1)
                            {
                                continue;
                            }//keep this if condition if you want to remove the first row
                        for($i = 0, $j = count($csv_line); $i < $j; $i++)
                            {
                                $insert_csv = array();
                                $insert_csv['january'] = $csv_line[0];
                                $insert_csv['february'] = $csv_line[1];
                                $insert_csv['march'] = $csv_line[2];
                                $insert_csv['april'] = $csv_line[3];
                                $insert_csv['may'] = $csv_line[4];
                                $insert_csv['june'] = $csv_line[5];
                                $insert_csv['july'] = $csv_line[6];
                                $insert_csv['august'] = $csv_line[7];
                                $insert_csv['september'] = $csv_line[8];
                                $insert_csv['october'] = $csv_line[9];
                                $insert_csv['november'] = $csv_line[10];
                                $insert_csv['december'] = $csv_line[11];
                            }
                        $i++;
                        $data = array(
                                      'id' => $id,
                                      'january' => $insert_csv['january'] ,
                                      'february' => $insert_csv['february'],
                                      'march' => $insert_csv['march'] ,
                                      'april' => $insert_csv['april'] ,
                                      'may' => $insert_csv['may'],
                                      'june' => $insert_csv['june'],
                                      'july' => $insert_csv['july'],
                                      'august' => $insert_csv['august'] ,
                                      'september' => $insert_csv['september'],
                                      'october' => $insert_csv['october'],
                                      'november' => $insert_csv['november'],
                                      'december' => $insert_csv['december'],
                                      );
                    };
                fclose($fp) or die("can't close file");
                $this->load->view("csv_upload_confirm",$data);
            }else {
                echo "ファイルがアップロードされていません。ファイルをアップロードしてください。";
                $data = array(
                              "id" => $id,
                              "nofileFlg" => "true"
                              );
                $this->load->view("csv_upload",$data);
            }
        } else {
            // 戻る
            $this->userDetailForAdmin($id);
        }
    }

    //csvアップロード登録・更新
    public function csv_upload_confirm(){

        $mode = $this->input->post("mode");
        $id = $this->input->post("id");
        $june = $this->input->post("june");
        $april = $this->input->post("april");
        $july = $this->input->post("july");

        if($mode == "アップロードする") {

            $this->load->model("model_users");

            // 決算ファイルを既に登録している場合
            if($this->model_users->count_kessan_data()){
                // 決算テーブルを更新する
                $this->model_users->update_kessan_data();
                // 決算ファイルを初めて登録する場合
            }else {
                // 決算テーブルを新規登録する
                $this->model_users->insert_kessan_data();
            }
            $this->load->view("csv_upload_complete");

        } else {
            // 確認画面に戻る
            $data=array(
                        "id"=> $id,
                        );
            $this->load->view("csv_upload",$data);
        }
    }

    //csvアップロード
    public function csv_upload_funds(){

        $mode = $this->input->post("mode");
        $id = $this->input->post("id");
        if($mode == "アップロードする") {

            // csvがアップロードされている場合
            if (!empty($_FILES['userfile']['name'])) {

                $count=0;
                $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
                while($csv_line = fgetcsv($fp,1024))
                    {
                        $count++;
                        if($count == 1)
                            {
                                continue;
                            }//keep this if condition if you want to remove the first row
                        for($i = 0, $j = count($csv_line); $i < $j; $i++)
                            {
                                $insert_csv = array();
                                $insert_csv['stock'] = $csv_line[0];
                                $insert_csv['investment'] = $csv_line[1];
                                $insert_csv['exchange'] = $csv_line[2];
                                $insert_csv['business'] = $csv_line[3];
                            }
                        $i++;
                        $data = array(
                                      'id' => $id,
                                      'stock' => $insert_csv['stock'] ,
                                      'investment' => $insert_csv['investment'],
                                      'exchange' => $insert_csv['exchange'] ,
                                      'business' => $insert_csv['business'] ,
                                      );
                    };
                fclose($fp) or die("can't close file");
                $this->load->view("csv_upload_funds_confirm",$data);
            }else {
                echo "ファイルがアップロードされていません。ファイルをアップロードしてください。";
                $data = array(
                              "id" => $id,
                              "nofileFlg" => "true"
                              );
                $this->load->view("csv_upload_funds",$data);
            }
        } else {
            // 戻る
            $this->userDetailForAdmin($id);
        }
    }

    //csvアップロード登録・更新
    public function csv_upload_funds_confirm(){

        $mode = $this->input->post("mode");
        $id = $this->input->post("id");

        if($mode == "アップロードする") {

            $this->load->model("model_users");

            // 資産状況ファイルを更新する  
            $this->model_users->update_funds_data();
            $this->load->view("csv_upload_funds_complete");

        } else {
            // 確認画面に戻る
            $data=array(
                        "id"=> $id,
                        );
            $this->load->view("csv_upload_funds",$data);
        }
    }

}