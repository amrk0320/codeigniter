<?php

class Model_users extends CI_Model{

    // ユーザーがログイン時に呼び出される
    public function can_log_in(){	//can_log_inメソッドを作っていく

        $this->db->where("email", $this->input->post("email"));	//POSTされたemailデータとDB情報を照合する
        $this->db->where("password", ($this->input->post("password")));	//POSTされたパスワードデータとDB情報を照合する
        $query = $this->db->get("users");

        if($query->num_rows() == 1){	//ユーザーが存在した場合の処理
            return true;
        }else{					//ユーザーが存在しなかった場合の処理
            return false;
        }
    }

    // 新規登録時に呼び出される
    public function add_users(){

        //add_temp_usersのモデルの実行時に、以下のデータを取得して、$dataと紐づける
        $data=array(
            "presidentname"=>$this->input->post("presidentname"),
            "presidentnamekana"=>$this->input->post("presidentnamekana"),
            "email"=>$this->input->post("email"),
            "password"=>($this->input->post("password"))
        );

        //$dataをDB内のtemp_usersに挿入したあとに、$queryと紐づける
        $query=$this->db->insert("users", $data);

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す
            return $data["email"]; 	//emailの値を返す
        }else{
            return false;
        }
    }

    // マイページでユーザー情報を取得時に呼び出される

    /**
     * @return array|bool
     */
    public function select_user_data(){

        $sql = "select  * from kessan k,funds f,users u where f.`userid` =  u.`id` and k.`userid` =  u.`id` and u.`email` = ? ";
        $query = $this->db->query($sql, array($_SESSION['email']));

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す

            if($query->num_rows() == 1){	//ユーザーが存在した場合の処理

                foreach ($query->result() as $r) {

                    $data=array(
                    "company"=> $r-> company,
                    "presidentname"=> $r-> presidentname,
                    "birthdt"=> $r-> birthdt,
                    "capitalstock"=> $r-> capitalstock,
                    "targetamount"=> $r-> targetamount,
                    "targetcontext"=> $r-> targetcontext,
                    "filename"=> $r-> pics,
                    "january"=> $r-> january,
                    "february"=> $r-> february,
                    "april"=> $r-> april,
                    "march"=> $r-> march,
                    "may"=> $r-> may,
                    "june"=> $r-> june,
                    "july"=> $r-> july,
                    "august"=> $r-> august,
                    "september"=> $r-> september,
                    "october"=> $r-> october,
                    "november"=> $r-> november,
                    "december"=> $r-> december,
                    "stock"=> $r-> stock,
                    "investment"=> $r-> investment,
                    "exchange"=> $r-> exchange,
                    "business"=> $r-> business,
                    );
                }
            }
            return $data; 	//取得した値を返す
        }else{
            return false;
        }
    }

    var $gallery_path;
    var $gallery_path_url;

    // 画像の格納(まだDBにアップではない)
    function do_upload(){

//        $this->gallery_path =  "/Applications/MAMP/htdocs/webApp/application/imageTmp";
        $this->gallery_path =  "/webApp/application/imageTmp";
        $this->gallery_path_url =  "../images/";

        $config = array(
            // ファイルのアップロード制限
            "allowed_types"=>"jpg|jpeg|gif|png",

            // ファイルのアップロード先を決める
            "upload_path"=>$this->gallery_path,

            // 3MB以上の画像は受け付けない
            "max_size" => 3000
        );

        // 第2引数で条件を受け取ることができます。
        $this->load->library("upload", $config);
        $this->upload->do_upload();

        // アップロードライブラリを読み込み、$image_dataに格納
        $image_data = $this->upload->data();

        // アップロードライブラリで生成された$image_dataから、
        // 以下のようにアップロードファイルのパスを取得できる
        $config = array(
            "source_image"=> $image_data["full_path"],
            "new_image" =>$this->gallery_path . "/thumbs",

            // リサイズされるときや、固定の値を指定したとき、もとの画像のアスペクト比を維持するかどうかを指定する
            "maintain_ratio"=>true,
            "width"=>150,
            "height"=>100
        );

        // イメージライブラリは次のように引数をとります。
        // 引数はライブラリ読み込み前に定義しておきます。
        $this->load->library("image_lib", $config);
        $this->image_lib->resize();
    }

    // マイページでユーザー情報を取得時に呼び出される
    public function update_userdata($data){

        $where = array(
            'email' => $_SESSION['email']
        );

        $this->db->where('email', $_SESSION['email']);
        $query = $this->db->update('users', $data);

        if($query){		//データ更新が成功したらTrue、失敗したらFalseを返す
            return true;
        }else{
            return false;
        }
    }

    // ユーザーidを取得する
    public function get_user_id($email){

        $where = array(
            'email' => $email
        );
        $query = $this->db->get_where('users', $where);

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() != 0){	//ユーザーが存在した場合の処理

                foreach ($query->result() as $r) {
                    $data= $r-> id;
                }
            }
            return $data; 	//取得した値を返す
        }else{
            return false;
        }
    }

    // 名前を取得する
    public function get_user_presidentname($email){

        $where = array(
            'email' => $email
        );
        $query = $this->db->get_where('users', $where);

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() != 0){	//ユーザーが存在した場合の処理

                foreach ($query->result() as $r) {
                    $data= $r-> presidentname;
                }
            }
            return $data; 	//取得した値を返す
        }else{
            return false;
        }
    }

    // ユーザー一覧データを取得する
    public function select_user_list(){

        $this->db->where('email !=', $_SESSION['email']);
        $query = $this->db->get('users');

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す

            if($query->num_rows() != 0){	//ユーザーが存在した場合の処理
                $data=array(
                    "data"=> $query->result()
                );
            } else {
                $data = array();
            }
            return $data; 	//取得した値を返す
        }else{
            return false;
        }
    }

        // いいね後に再度、詳細ページをリロードするときに呼び出す。select_user_datailとの違いは"nowGoodFlg"=> "true"を付与している。
    // nowGoodFlgがあれば画面で「いいねしました。」と出す。
    public function select_user_datail_after_good($id){

        $sql = "select f.`business`,f.`investment`,f.`exchange`,f.`stock`,k.`january`,k.`february`,k.`march`,k.`april`,,k.`may`,k.`june`,k.`july`,k.`august`,k.`september`,k.`october`,k.`november`,k.`december`,count(g.fromuserid) as good,`mygood`, u.`id`,u.`email`, u.`password`, u.`company`, u.`presidentname`, u.`birthdt`, u.`capitalstock`, u.`targetamount`, u.`targetcontext`, u.`pics` from users u,good g,(select count(*) as mygood from good where `touserid` = ? and `fromuserid` = ?) m ,(select * from kessan where userid = ?) k ,(select * from funds where userid = ?) f where g.`touserid` = ? and u.`id` = ?";
        $query = $this->db->query($sql, array($id, $_SESSION['id'], $id,$id,  $id));

        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() == 1){    //ユーザーが存在した場合の処理
                foreach ($query->result() as $r) {
                    $data=array(
                        "id"=> $r-> id,
                        "good"=> $r-> good,
                        "mygood"=> $r-> mygood,
                        "email"=> $r-> email,
                        "company"=> $r-> company,
                        "presidentname"=> $r-> presidentname,
                        "birthdt"=> $r-> birthdt,
                        "capitalstock"=> $r-> capitalstock,
                        "targetamount"=> $r-> targetamount,
                        "targetcontext"=> $r-> targetcontext,
                        "filename"=> $r-> pics,
                        "nowGoodFlg"=> "true",
                        "january"=> $r-> january,
                        "february"=> $r-> february,
                        "april"=> $r-> april,
                        "march"=> $r-> march,
                        "may"=> $r-> may,
                        "june"=> $r-> june,
                        "july"=> $r-> july,
                        "august"=> $r-> august,
                        "september"=> $r-> september,
                        "october"=> $r-> october,
                        "november"=> $r-> november,
                        "december"=> $r-> december,
                        "stock"=> $r-> stock,
                        "investment"=> $r-> investment,
                        "exchange"=> $r-> exchange,
                        "business"=> $r-> business,
                    );
                }
            }
            return $data;   //取得した値を返す
        }else{
            return false;
        }
    }

    public function select_user_datail($id){
        $sql = "select f.`business`,f.`investment`,f.`exchange`,f.`stock`,k.`january`,k.`february`,k.`march`,k.`april`,k.`may`,k.`june`,k.`july`,k.`august`,k.`september`,k.`october`,k.`november`,k.`december`,count(g.fromuserid) as good,`mygood`, u.`id`,u.`email`, u.`password`, u.`company`, u.`presidentname`, u.`birthdt`, u.`capitalstock`, u.`targetamount`, u.`targetcontext`, u.`pics` from users u,good g,(select count(*) as mygood from good where `touserid` = ? and `fromuserid` = ?) m ,(select * from kessan where userid = ?) k ,(select * from funds where userid = ?) f where g.`touserid` = ? and u.`id` = ?";
        $query = $this->db->query($sql, array($id, $_SESSION['id'], $id,$id,  $id,$id));

        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() == 1){    //ユーザーが存在した場合の処理
                foreach ($query->result() as $r) {
                    $data=array(
                        "id"=> $r-> id,
                        "good"=> $r-> good,
                        "mygood"=> $r-> mygood,
                        "email"=> $r-> email,
                        "company"=> $r-> company,
                        "presidentname"=> $r-> presidentname,
                        "birthdt"=> $r-> birthdt,
                        "capitalstock"=> $r-> capitalstock,
                        "targetamount"=> $r-> targetamount,
                        "targetcontext"=> $r-> targetcontext,
                        "filename"=> $r-> pics,
                        "nowGoodFlg"=> "false",
                        "january"=> $r-> january,
                        "february"=> $r-> february,
                        "april"=> $r-> april,
                        "march"=> $r-> march,
                        "may"=> $r-> may,
                        "june"=> $r-> june,
                        "july"=> $r-> july,
                        "august"=> $r-> august,
                        "september"=> $r-> september,
                        "october"=> $r-> october,
                        "november"=> $r-> november,
                        "december"=> $r-> december,
                        "stock"=> $r-> stock,
                        "investment"=> $r-> investment,
                        "exchange"=> $r-> exchange,
                        "business"=> $r-> business,
                    );
                }
            }
            return $data;   //取得した値を返す
        }else{
            return false;
        }
    }

    // いいねをする
    public function add_good($id){

        $data=array(
            "fromuserid"=>$_SESSION['id'],
            "touserid"=>$id
        );

        //$dataをDB内のtemp_usersに挿入したあとに、$queryと紐づける
        $query=$this->db->insert("good", $data);

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す
            return true;
        }else{
            return false;
        }
    }

    // 自分にいいねしたユーザーの一覧を取得する
    public function select_good_user_list(){

        $sql = "select u.`id`,u.`presidentname`,u.`company`,u.`birthdt`,u.`capitalstock`,u.`targetamount`,u.`targetcontext` from users u,good g WHERE g.`touserid` = ? AND g.`fromuserid` = u.`id` ORDER BY g.`createtm`";
        $query = $this->db->query($sql, array($_SESSION['id']));

        if($query){		//データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() >= 1){	//ユーザーが存在した場合の処理
                    $data=array(
                        "data"=> $query->result()
                    );
            }
                return $data; 	//取得した値を返す
        } else {
            return false;
        }
    }

       // 自分にいいねしたユーザーの一覧を取得する
    public function select_good_shita_user_list(){

        $sql = "select u.`id`,u.`presidentname`,u.`company`,u.`birthdt`,u.`capitalstock`,u.`targetamount`,u.`targetcontext` from users u,good g WHERE g.`fromuserid` = ? AND g.`touserid` = u.`id` ORDER BY g.`createtm`";
        $query = $this->db->query($sql, array($_SESSION['id']));

        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() >= 1){    //ユーザーが存在した場合の処理
                    $data=array(
                        "data"=> $query->result()
                    );
            }
                return $data;   //取得した値を返す
        } else {
            return false;
        }
    }

    // 新規登録時デフォルト決算データを登録する
    public function insert_kessan_data_first($id){

        $data=array(
            "userid"=> $id,
            "january"=> 0,
            "february"=> 0,
            "april"=> 0,
            "march"=> 0,
            "may"=> 0,
            "june"=> 0,
            "july"=> 0,
            "august"=> 0,
            "september"=> 0,
            "october"=> 0,
            "november"=> 0,
            "december"=> 0,
        );

        $query=$this->db->insert("kessan", $data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

   // 新規登録時デフォルト決算データを登録する
    public function insert_funds_data_first($id){

        $data=array(
            "userid"=> $id,
            "stock"=> 0,
            "investment"=> 0,
            "exchange"=> 0,
            "business"=> 0,
        );

        $query=$this->db->insert("funds", $data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

      // ユーザーが管理画面にログイン時に呼び出される
    public function can_admin_log_in(){

        $this->db->where("userid", $this->input->post("userid"));   //POSTされたemailデータとDB情報を照合する
        $this->db->where("password", ($this->input->post("password"))); //POSTされたパスワードデータとDB情報を照合する
        $query = $this->db->get("adminusers");

        if($query->num_rows() == 1){    //ユーザーが存在した場合の処理
            return true;
        }else{                  //ユーザーが存在しなかった場合の処理
            return false;
        }
    }

    // ユーザー一覧データを取得する
    public function select_user_list_for_admin(){

        $query = $this->db->get('users');

        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す

            if($query->num_rows() != 0){    //ユーザーが存在した場合の処理
                $data=array(
                    "data"=> $query->result()
                );
            } else {
                $data = array();
            }
            return $data;   //取得した値を返す
        }else{
            return false;
        }
    }

    public function select_user_datail_for_admin($id){
        $sql = "select f.`business`,f.`investment`,f.`exchange`,f.`stock`,k.`january`,k.`february`,k.`march`,k.`april`,k.`may`,k.`june`,k.`july`,k.`august`,k.`september`,k.`october`,k.`november`,k.`december`,count(g.fromuserid) as good,u.`id`,u.`email`, u.`password`, u.`company`, u.`presidentname`, u.`birthdt`, u.`capitalstock`, u.`targetamount`, u.`targetcontext`, u.`pics` from users u,good g ,(select * from kessan where userid = ?) k,(select * from funds where userid = ?) f where g.`touserid` = ? and u.`id` = ?";
        $query = $this->db->query($sql, array($id,$id,$id,$id));

        if($query){     //データ取得が成功したらTrue、失敗したらFalseを返す
            if($query->num_rows() == 1){    //ユーザーが存在した場合の処理
                foreach ($query->result() as $r) {
                    $data=array(
                        "id"=> $r-> id,
                        "good"=> $r-> good,
                        "email"=> $r-> email,
                        "company"=> $r-> company,
                        "presidentname"=> $r-> presidentname,
                        "birthdt"=> $r-> birthdt,
                        "capitalstock"=> $r-> capitalstock,
                        "targetamount"=> $r-> targetamount,
                        "targetcontext"=> $r-> targetcontext,
                        "filename"=> $r-> pics,
                        "january"=> $r-> january,
                        "february"=> $r-> february,
                        "april"=> $r-> april,
                        "march"=> $r-> march,
                        "may"=> $r-> may,
                        "june"=> $r-> june,
                        "july"=> $r-> july,
                        "august"=> $r-> august,
                        "september"=> $r-> september,
                        "october"=> $r-> october,
                        "november"=> $r-> november,
                        "december"=> $r-> december,
                        "stock"=> $r-> stock,
                        "investment"=> $r-> investment,
                        "exchange"=> $r-> exchange,
                        "business"=> $r-> business,
                        "nowGoodFlg"=> "false",
                    );
                }
            }
            return $data;   //取得した値を返す
        }else{
            return false;
        }
    }

    public function delete_user_data(){
        $this->db->where("id", $this->input->post("id"));
        $query = $this->db->delete("users");
        if($query){ //ユーザーが存在した場合の処理
            return true;
        }else{                  //ユーザーが存在しなかった場合の処理
            return false;
        }
    }

    public function count_kessan_data(){
        $this->db->where("userid", $this->input->post("id"));
        $query = $this->db->get("kessan");

        if($query->num_rows() != 0){
            return true;
        } else {
            return  false;
        }
    }

    public function insert_kessan_data(){

        $data=array(
            "userid"=> $this->input->post("id"),
            "january"=> $this->input->post("january"),
            "february"=> $this->input->post("february"),
            "march"=> $this->input->post("march"),
            "april"=> $this->input->post("april"),
            "may"=> $this->input->post("may"),
            "june"=> $this->input->post("june"),
            "july"=> $this->input->post("july"),
            "april"=> $this->input->post("april"),
            "september"=> $this->input->post("september"),
            "october"=> $this->input->post("october"),
            "november"=> $this->input->post("november"),
            "december"=> $this->input->post("december"),
        );

        $query=$this->db->insert("kessan", $data);
        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function update_kessan_data(){

        $data=array(
            "january"=> $this->input->post("january"),
            "february"=> $this->input->post("february"),
            "march"=> $this->input->post("march"),
            "april"=> $this->input->post("april"),
            "may"=> $this->input->post("may"),
            "june"=> $this->input->post("june"),
            "july"=> $this->input->post("july"),
            "august"=> $this->input->post("august"),
            "september"=> $this->input->post("september"),
            "october"=> $this->input->post("october"),
            "november"=> $this->input->post("november"),
            "december"=> $this->input->post("december"),
        );

        $this->db->where("userid", $this->input->post("id"));
        $query = $this->db->update('kessan', $data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function update_funds_data(){

        $data=array(
            "stock"=> $this->input->post("stock"),
            "investment"=> $this->input->post("investment"),
            "exchange"=> $this->input->post("exchange"),
            "business"=> $this->input->post("business"),
        );

        $this->db->where("userid", $this->input->post("id"));
        $query = $this->db->update('funds', $data);

        if($query){
            return true;
        }else{
            return false;
        }
    }
}

?>