<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<div class="wrap register">
    <div class="in">
        <h2>管理者ログイン画面</h2>
        <?php
        echo form_open("main/adminlogin_validation/");	//フォームを開く
        echo validation_errors();		//バリデーションがあればエラーを出す
        ?>
        <table class="company">
            <tbody>
            <tr>
                <th>ユーザーID</th>
                <td>
                    <?php
                    $userid = array(
                        'name'        => 'userid',
                        'value'          => $this->input->post('userid'),
                        'class'       => '',
                    );
                    echo form_input($userid);
                    ?>
                </td>
            </tr>
            <tr>
                <th>パスワード</th>
                <td>
                    <?php
                    $password = array(
                        'name'        => 'password',
                        'value'          => $this->input->post('password'),
                        'class'       => '',
                    );
                    echo form_password($password);	//パスワードの入力フィールドを出力
                    ?>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="btn gray hover login">
            <?php
            echo form_submit("login_submit", "ログイン","class='btn gray hover login'");  //ユーザー登録ボタンを出力
            echo form_close();	//フォームを閉じる
            ?>
        </div>
    </div>
</div>
<footer>
    Copyright 会社を創ろう.com 2017 all rights reserved.
</footer>
</body>
</html>
