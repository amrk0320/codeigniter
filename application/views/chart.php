<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
     var data = google.visualization.arrayToDataTable([
         ['月', '売上'],
         ['1月',  <?php if(isset($january)){ echo $january; } else { echo 0; } ?>],
         ['2月',  <?php if(isset($february)){ echo $february; } else { echo 0; } ?>],
         ['3月',  <?php if(isset($march)){ echo $march; } else { echo 0; } ?>],
         ['4月',  <?php if(isset($april)){ echo $april; } else { echo 0; } ?>],
         ['5月',  <?php if(isset($may)){ echo $may; } else { echo 0; } ?>],
         ['6月',  <?php if(isset($june)){ echo $june; } else { echo 0; } ?>],
         ['7月',  <?php if(isset($july)){ echo $july; } else { echo 0; } ?>],
         ['8月',  <?php if(isset($august)){ echo $august; } else { echo 0; } ?>],
         ['9月',  <?php if(isset($september)){ echo $september; } else { echo 0; } ?>],
         ['10月',  <?php if(isset($october)){ echo $october; } else { echo 0; } ?>],
         ['11月',  <?php if(isset($november)){ echo $november; } else { echo 0; } ?>],
         ['12月',  <?php if(isset($december)){ echo $december; } else { echo 0; } ?>],
     ]);
     var options = {
         title: '会社業績'
     };
     var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
     chart.draw(data, options);
 }
</script>

<script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 //円グラフ
 function drawChart() {
     var data = google.visualization.arrayToDataTable([ 
         //グラフデータの指定
         ['Task', '運用資金の状況'],
         ['株',     11],
         ['投信',      2],
         ['為替',  2],
         ['事業等', 2],
     ]);
     var options = { //オプションの指定
                     pieSliceText: 'label',
                     title: '運用資金の状況'
     };
     var chart = new google.visualization.PieChart(document.getElementById('piechart')); //グラフを表示させる要素の指定
     chart.draw(data, options);
 }
</script>
