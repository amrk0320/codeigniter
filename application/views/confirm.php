<?php include("header-signup.php") ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>

<div class="container">
  
  <div class="row">
  </div>
  <div class="row">
    <div class="container center">
      <div class="col s12 orange-text darken-4">
        <h4>会員登録確認</h4>
      </div>
      <?php
      $attributes = array('class' => 'col s12');
      echo form_open("main/complete",$attributes);
      //echo validation_errors();
      ?>
      <form class="col s12">
        <div class="row">
        </div>
        <div class="row">
          <div class="col s12 orange white-text">お名前</div>
          <div class="col s12">
            <?php
            echo $presidentname;
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col s12 orange white-text">フリガナ</div>
          <div class="col s12">
            <?php
            echo $presidentnamekana;
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col s12 orange white-text">メールアドレス</div>
          <div class="col s12">
            <?php
            echo $email;
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col s12 orange white-text">パスワード</div>
          <div class="col s12">
            <?php
            echo $passwordango;
            ?>
          </div>
        </div>
        <?php
        $data = array(
            'presidentname' => $presidentname,
            'presidentnamekana' => $presidentnamekana,
            'cemail' => $cemail,
            'email' => $email,
            'password'  => $password
        );
        echo form_hidden($data);
        echo form_submit("mode", "戻る","class='waves-effect waves-light btn-large grey lighten-1'");
        echo form_submit("mode", "登録","class='waves-effect waves-light btn-large orange'");
        echo form_close();  //フォームを閉じる
        ?>
        <!--<a href="signup.html" class="waves-effect waves-light btn-large grey lighten-1">戻る</a><a href="signup_done.html" class="waves-effect waves-light btn-large orange">OK</a>-->
    </div>
  </div>

</font>
</body>
</html>
