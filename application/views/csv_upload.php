<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<div class="wrap register">
    <div class="in">
    		<?php if(isset($_SESSION['is_admin_logged_in'])){ ?>
        <h1>決算書をアップロードしてください。</h1>
        <h1>既にアップロードしている場合は、データが更新されます。</h1>
        <?php if(isset($nofileFlg)){ ?>
        <h1>※ファイルが選択されていません。アップロードするファイルを選択してください。</h1>
        <?php }?>
            <?php
    echo form_open_multipart("main/csv_upload");
    $data=array(
        "id"=> $id,
    );
    echo form_hidden($data);
    echo "</br></br>";
    echo form_upload("userfile");
    echo form_submit("mode", "アップロードする");
    echo form_submit("mode", "戻る"); //
    echo "</p>";
    ?>
        <?php }?>
    </div>
    <footer>
        Copyright 会社を創ろう.com 2017 all rights reserved.
    </footer>
</body>
</html>
