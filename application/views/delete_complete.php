<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<div class="wrap register">
    <div class="in">
    		<?php if(isset($_SESSION['is_admin_logged_in'])){ ?>
        <h1>ユーザーの退会処理が完了しました。</h1>
        <a href="<?php echo site_url('main/admin_members') ?>">ユーザー一覧へ戻る</a>
        <?php }?>
    </div>
    <footer>
        Copyright 会社を創ろう.com 2017 all rights reserved.
    </footer>
</body>
</html>
