<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<div class="wrap top">
    <div class="in clearfix">
        <div class="chart">
            <p class="like"><img src="/webApp/img/logo.png">×<?php echo $good ?>
            </p>
        </div>
        <table class="detail">
            <tbody>
            <tr>
                <th>会社名</div></th>
    <td><?php $company?></td>
    </tr>
    <tr>
        <th>代表者名</th>
        <td>
            <?php echo $presidentname?>
        </td>
    </tr>
    <tr>
        <th>設立年月日</th>
        <td>
            <?php   if(strlen($birthdt) > 1):?>
                <?php echo substr($birthdt, 0, 4)?>年<?php echo substr($birthdt, 4, 2)?>月<?php echo substr($birthdt, 6, 2)?>日
            <?php else: ?>
                設立年月日が未登録です。
            <?php endif;?>
        </td>
    </tr>
    <tr>
        <th>資本金</th>
        <td>
            <?php echo $capitalstock?>円
        </td>
    </tr>
    <tr>
        <th>目標金額</th>
        <td>
            <?php echo $targetamount?>円
        </td>
    </tr>
    <tr>
        <th>目的</th>
        <td>
            <?php echo $targetcontext?>
        </td>
    </tr>
    <tr>
        <th>写真</th>
        <td>
            <?php if($filename != ""):?>
                <img src="<?php echo "http://localhost:8888/webApp/uploads/".$filename; ?>" width="200px" height="200px">
            <?php else: ?>
                画像がアップロードされていません。
            <?php endif;?>
        </td>
    </tr>
    </tbody>
    </table>
    <?php
    echo form_open("main/detail_admin_complete");
    $data=array(
        "id"=> $id,
    );
    echo form_hidden($data);
    echo "<p>";
    echo "<p>本当に退会させてよろしいでしょうか？</br>";
    echo form_submit("mode", "退会させる");
    echo form_submit("mode", "戻る"); //
    echo "</p>";

    ?>
</div>
</div>
<footer>
    Copyright 会社を創ろう.com 2017 all rights reserved.
</footer>
</body>
</html>

