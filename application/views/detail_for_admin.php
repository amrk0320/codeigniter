<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
   <script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
     var data = google.visualization.arrayToDataTable([
         ['月', '売上'],
         ['1月',  <?php if(isset($january)){ echo $january; } else { echo 0; } ?>],
         ['2月',  <?php if(isset($february)){ echo $february; } else { echo 0; } ?>],
         ['3月',  <?php if(isset($march)){ echo $march; } else { echo 0; } ?>],
         ['4月',  <?php if(isset($april)){ echo $april; } else { echo 0; } ?>],
         ['5月',  <?php if(isset($may)){ echo $may; } else { echo 0; } ?>],
         ['6月',  <?php if(isset($june)){ echo $june; } else { echo 0; } ?>],
         ['7月',  <?php if(isset($july)){ echo $july; } else { echo 0; } ?>],
         ['8月',  <?php if(isset($august)){ echo $august; } else { echo 0; } ?>],
         ['9月',  <?php if(isset($september)){ echo $september; } else { echo 0; } ?>],
         ['10月',  <?php if(isset($october)){ echo $october; } else { echo 0; } ?>],
         ['11月',  <?php if(isset($november)){ echo $november; } else { echo 0; } ?>],
         ['12月',  <?php if(isset($december)){ echo $december; } else { echo 0; } ?>],
     ]);
     var options = {
         title: '会社業績'
     };
     var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
     chart.draw(data, options);
 }
</script>
<script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
     var data = google.visualization.arrayToDataTable([ 
         //グラフデータの指定
        <?php if($stock == 0 && $business == 0 && $exchange == 0 && $investment == 0 ){ 
        ?>
        ['Task', '運用資金の状況'],
        ['登録なし',     1],
        <?php 
        } 
        else
        { 
        ?>
        ['Task', '運用資金の状況'],
        ['株',     <?php echo $stock; ?>],
        ['投信',    <?php  echo $investment; ?>],
        ['為替',  <?php echo $exchange; ?>],
        ['事業等', <?php echo $business; ?>],
        <?php 
        } 
       ?>
     ]);
     var options = { //オプションの指定
                     pieSliceText: 'label',
                     title: '運用資金の状況'
     };
     var chart = new google.visualization.PieChart(document.getElementById('piechart')); //グラフを表示させる要素の指定
     chart.draw(data, options);
 }
</script>
</head>
<body>
<header id="top">
    <div class="in clearfix">
        <div class="left">
            <h1><a class="hover" href="./"><img src="/webApp/img/logo.png" alt="会社を創ろう.com"></a></h1>
        </div>
        <div class="right clearfix">
            <div class="btns clearfix">
                <p class="btn blue fltleft hover"><a href="<?php echo site_url('main/signup') ?>">会員登録する</a></p>
                <p class="btn gray fltleft hover"><a href="<?php echo site_url('main/login') ?>">ログイン</a></p>
            </div>
        </div>
    </div>
</header>
<div class="wrap top">
    <div class="in clearfix">
        <div class="chart">
        <div id="chart_div" style="width: 100%; height:300px;"></div>
        <div id="piechart"></div>
            <p class="like"><img src="/webApp/img/logo.png">×<?php echo $good ?>
            </p>
        </div>
        <table class="detail">
            <tbody>
            <tr>
                <th>会社名</div></th>
    <td><?php $company?></td>
    </tr>
    <tr>
        <th>代表者名</th>
        <td>
            <?php echo $presidentname?>
        </td>
    </tr>
    <tr>
        <th>設立年月日</th>
        <td>
            <?php   if(strlen($birthdt) > 1):?>
                <?php echo substr($birthdt, 0, 4)?>年<?php echo substr($birthdt, 4, 2)?>月<?php echo substr($birthdt, 6, 2)?>日
            <?php else: ?>
                設立年月日が未登録です。
            <?php endif;?>
        </td>
    </tr>
    <tr>
        <th>資本金</th>
        <td>
            <?php echo $capitalstock?>円
        </td>
    </tr>
    <tr>
        <th>目標金額</th>
        <td>
            <?php echo $targetamount?>円
        </td>
    </tr>
    <tr>
        <th>目的</th>
        <td>
            <?php echo $targetcontext?>
        </td>
    </tr>
    <tr>
        <th>写真</th>
        <td>
            <?php if($filename != ""):?>
                <img src="<?php echo "http://localhost:8888/webApp/uploads/".$filename; ?>" width="200px" height="200px">
            <?php else: ?>
                画像がアップロードされていません。
            <?php endif;?>
        </td>
    </tr>
    </tbody>
    </table>
    <?php
    echo form_open("main/detail_admin_form");
    $data=array(
        "id"=> $id,
    );
    echo form_hidden($data);
    echo "<p>";
    echo form_submit("mode", "決算書をアップロードする");
    echo form_submit("mode", "資産状況をアップロードする");
    echo form_submit("mode", "退会させる");
    echo form_submit("mode", "戻る"); //
    echo "</p>";

    ?>
</div>
</div>
<footer>
    Copyright 会社を創ろう.com 2017 all rights reserved.
</footer>
</body>
</html>

