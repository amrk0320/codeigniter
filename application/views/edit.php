<?php include("header-signup.php") ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>

<div class="container">
  
  <div class="row">
  </div>
  <div class="row">
    <div class="container center">
      <div class="col s12 orange-text darken-4">
        <h4>マイページ編集</h4>
      </div>
      <?php
      $attributes = array('class' => 'col s12');
      echo form_open("main/edit_validation", $attributes);
      ?>
      <div class="row">
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("company", $this->input->post("company"),"class='validate'");?>
          <label for="company">会社名</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("presidentname", $this->input->post("presidentname"),"class='validate'");?>
          <label for="presidentname">代表取締役社長</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("birthdt", $this->input->post("birthdt"),"class='validate'");?>
          <label for="birthdt">設立日(例:20060501)</label>
        </div>
        <?php echo form_error('birthdt', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("capitalstock", $this->input->post("capitalstock"),"class='validate'");?>
          <label for="capitalstock">資本金(例:3000000 *カンマ無し)</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("targetamount", $this->input->post("targetamount"),"class='validate'");?>
          <label for="targetstock">目標金額(例:10000000 *カンマ無し)</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php echo form_input("targetcontext", $this->input->post("targetcontext"),"class='validate'");?>
          <label for="targetstock">概要</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php if($filename != ""):?>
            <img style="max-width:100%;" src="<?php echo site_url();?>uploads/<?php echo $filename;?>">
            <?php echo "<input class='validate' value='画像を更新する場合は再度、写真をアップしてください。' disabled='disabled' type='text'>";?>
          <?php else: ?>
            <?php echo "<input class='validate' value='画像が登録されていません。' disabled='disabled' type='text'>";?>
          <?php endif;?>
          <?php if($overFlg == "1"){?>
            <font color="red">画像がサイズが大きすぎます。1000×1000ピクセル以内の画像をアップロードしてください。</font>
          <?php }?>
                </br>
                <?php echo form_upload("userfile");?>
                <label for="filename">社章</label>
        </div>
      </div>
      <?php
      echo form_submit("mode", "確認","class='waves-effect waves-light btn-large orange'");
      ?>
      <?php
      $data=array(
          "beforefilename"=> $filename,
      );
      echo form_hidden($data);
      ?>
    </div>
  </div>

</div><!-- container -->

        </font>
</body>
</html>
