<?php include("header-signup.php") ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>

<div class="container">

  <div class="row">
  </div>
  
  <div class="row">
    <div class="container center">
      <div class="col s12 orange-text darken-4">
        <h4>マイページ編集確認</h4>
      </div>
      <?php
      echo form_open("main/edit_confirm");
      ?>
      <div class="row">
        <div class="section">
          <h6>会社名<?php echo $company; ?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>代表取締役社長<?php echo $presidentname; ?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>設立日<?php if(strlen($birthdt) > 1):?>
            <?php echo substr($birthdt, 0, 4)?>年<?php echo substr($birthdt, 4, 2)?>月<?php echo substr($birthdt, 6, 2)?>日
          <?php else: ?>
            設立日が未登録です。
          <?php endif;?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>資本金<?php echo $capitalstock; ?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>目標金額<?php echo $targetamount; ?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>概要<?php echo $targetcontext; ?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>社章<?php if(isset($images[0]['thumb_url']) && count($images) > 0 ):?>
            <img src="<?php echo site_url(); echo $images[0]['thumb_url'];?>">
          <?php else: ?>
            画像がアップロードされていません。
          <?php endif;?></h6>
        </div>
        <div class="divider"></div>
        </div><!-- row -->
        <?php
        echo form_submit("mode", "戻る","class='waves-effect waves-light btn-large grey lighten-1'"); //会員登録ボタン
        echo form_submit("mode", "更新","class='waves-effect waves-light btn-large orange'");   //会員登録ボタン
        ?>
        <?php

        $data=array(
            "company"=> $company,
            "presidentname"=> $presidentname,
            "birthdt"=> $birthdt,
            "capitalstock"=> $capitalstock,
            "targetamount"=> $targetamount,
            "targetcontext"=> $targetcontext,
            "filename"=> $filename,
            "beforefilename"=> $beforefilename
        );
        echo form_hidden($data);
        ?>
    </div><!-- container -->
  </div><!-- row -->
</div><!-- container -->
</font>
</body>
        </html>
