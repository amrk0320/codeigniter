<?php include("header-signup.php"); ?>
<body>
<font face="Hiragino Maru Gothic Pro">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>

    <nav class="orange lighten-5" role="navigation">
        <div class="nav-wrapper container">
          <a id="logo-container" href="<?php echo site_url(); ?>" class="brand-logo"><img src="<?php echo site_url(); ?>img/logo.png" alt="Unsplashed background img 1" height="52" width="72"></a>
            <ul class="right hide-on-med-and-down">
                <li><a class ="orange-text darken-4" href="demo.html">使い方</a></li>
                <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/mypage">マイページ</a></li>
                <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/logout">ログアウト</a></li>
                <li><a href="mailto:info＠sentimental.co.jp"><font color="orange">お問い合わせ</font></a></li>
            </ul>

            <ul id="nav-mobile" class="side-nav orange-text">
                <li><a class ="orange-text darken-4" href="demo.html">使い方</a></li>
                <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/mypage">マイページ</a></li>
                <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/logout">ログアウト</a></li>
                <li><a href="mailto:info＠sentimental.co.jp?subject=&amp;body=">お問い合わせ</a></li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    
    
    <div class="container">
        <div class="row">
        </div>
        <div class="row">
            <div class="center">
                <div class="col s12 orange-text darken-4">
                    <h4>いいねした会社</h4>
                </div>
                <div class="row show-on-small hide-on-med-and-up">
                    <div class="col s12 row"><a href="index2.html" style="width: 300px" class="waves-effect waves-light orange btn">会社一覧</a></div>
                    <div class="col s12 row"><a href="iine.html" style="width: 300px" class="waves-effect waves-light orange btn">いいねした会社</a></div>
                    <div class="col s12"><a href="friend.html" style="width: 300px" class="waves-effect waves-light orange btn">いいねしてくれた会社</a></div>

                </div>
                <div class="row show-on-medium-and-up hide-on-small-only">
                    <div class="row">
                        <a href="index2.html" style="width: 200px" class="waves-effect waves-light orange btn">会社一覧</a>
                        <a href="iine.html" style="width: 200px" class="waves-effect waves-light orange btn">いいねした会社</a>
                        <a href="friend.html" style="width: 200px" class="waves-effect waves-light orange btn">いいねしてくれた会社</a>
                    </div>
                </div>
            </div>

            <div class="row">
    
      <?php foreach($data as $test) { ?>
        <?php if($test->goodcount != 0){ ?>
          <?php foreach($data as $test) { ?>
            <div class="col s12 m4">
                    <div class="card orange lighten-5">
                        <div class="card-image">
                            <a href="<?php echo site_url('main/userDetail/'.$test->id) ?>"><img src="<?php echo site_url(); ?>img/card.png"></a>
                        </div>
                        <div class="card-content">
                            <ul class="collection with-header">
                                <li class="collection-header"><h5>株式会社 ○○</h5></li>
                                <li class="collection-item light-green lighten-5">代表者名　Test</li>
                                <li class="collection-item">設立日 2017年--月--日</li>
                                <li class="collection-item light-green lighten-5">資本金 --</li>
                                <li class="collection-item">目標金額 --</li>
                                <li class="collection-item light-green lighten-5">概要 --</li>
                            </ul>
                        </div>
                    </div>
                </div>
            <!--<tr>
              <td>名前:<a class="good_user_name" href="<?php echo site_url('main/userDetail/'.$test->id) ?>"><?php echo $test->presidentname;?>さん</a></td>
              <td><?php echo $test->createtm;?></td>
            </tr>-->
          <?php }?>
        <?php }else{;?>
          <?php echo "いいねしてくれたユーザーはまだいません";?>
        <?php }?>
      <?php }?>
            </div>
            </div>
</font>
</body>
        </html>
