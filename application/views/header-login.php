<nav class="orange lighten-5" role="navigation">
  <div class="nav-wrapper container">

    <?php if(isset($_SESSION['email'])){ ?>
    
    <a id="logo-container" href="<?php echo site_url(); ?>" class="brand-logo"><img src="<?php echo site_url(); ?>img/logo.png" alt="Unsplashed background img 1" height="52" width="72"></a>
    <ul class="right hide-on-med-and-down">
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/demo">使い方</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/mypage">マイページ</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/logout">ログアウト</a></li>
      <li><a class ="orange-text darken-4" href="mailto:info＠sentimental.co.jp">お問い合わせ</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav orange-text">
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/demo">使い方</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/mypage">マイページ</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/logout">ログアウト</a></li>
      <li><a class ="orange-text darken-4" href="mailto:info＠sentimental.co.jp">お問い合わせ</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>

    <?php }else{ ?>

      <a id="logo-container" href="<?php echo site_url(); ?>" class="brand-logo"><img src="<?php echo site_url(); ?>img/logo.png" alt="Unsplashed background img 1" height="52" width="72"></a>
    <ul class="right hide-on-med-and-down">
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/demo">使い方</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/signup">会員登録</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/login">ログイン</a></li>
      <li><a class="orange-text darken-4" href="mailto:info＠sentimental.co.jp">お問い合わせ</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav orange-text">
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/demo">使い方</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/signup">会員登録</a></li>
      <li><a class ="orange-text darken-4" href="<?php echo site_url(); ?>main/login">ログイン</a></li>
      <li><a class="orange-text darken-4" href="mailto:info＠sentimental.co.jp">お問い合わせ</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>

    <?php }; ?>
    
  </div>
</nav>
