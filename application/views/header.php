<nav class="blue darken-1" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="<?php echo site_url(); ?>" class="brand-logo"><img src="<?php echo site_url();?>img/logo.png" alt="Unsplashed background img 1" height="52" width="72"></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="<?php echo site_url(); ?>main/demo"><font color="white">使い方</font></a></li>
      <li><a href="<?php echo site_url(); ?>main/signup"><font color="white">会員登録</font></a></li>
      <li><a href="<?php echo site_url(); ?>main/login"><font color="white">ログイン</font></a></li>
      <li><a href="mailto:info＠sentimental.co.jp"><font color="white">お問い合わせ</font></a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav">
      <li><a href="<?php echo site_url(); ?>main/demo">使い方</a></li>
      <li><a href="<?php echo site_url(); ?>main/signup">会員登録</a></li>
      <li><a href="<?php echo site_url(); ?>main/login">ログイン</a></li>
      <li><a href="mailto:info＠sentimental.co.jp">問い合わせ</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i id="welcome_menu" class="material-icons">menu</i></a>
  </div>
</nav>
