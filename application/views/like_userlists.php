<div class="col s12 m4">
  <?php var_dump($test); ?>
  <a href="<?php echo site_url('main/userDetail/'.$test->id) ?>">
    <div class="card orange lighten-5">
      <div class="card-image">
        <?php if($test->pics != ""):?>
          <img src="<?php echo site_url();?>/uploads/<?php echo $test->pics; ?>">
        <?php else: ?>
          No Image.
        <?php endif;?>
        <!--<a href="<?php echo site_url('main/userDetail/'.$test->id) ?>">-->
        <!--<img src="<?php echo site_url(); ?>img/card.png">-->
        <!--</a>-->
      </div>
      <div class="card-content">
        <ul class="collection with-header">
          <li class="collection-header"><h5><?php echo $test->company;?></h5></li>
          <li class="collection-item light-green lighten-5">代表者取締役社長　<?php echo $test->presidentname;?></li>
          <li class="collection-item">設立日 <?php echo substr($test->birthdt, 0, 4)?>年<?php echo substr($test->birthdt, 4, 2)?>月<?php echo substr($test->birthdt, 6, 2)?>日</li>
          <li class="collection-item light-green lighten-5">資本金 <?php echo $test->capitalstock;?>円</li>
          <li class="collection-item">目標金額 <?php echo $test->targetamount;?>円</li>
          <li class="collection-item light-green lighten-5">概要 <?php echo $test->targetcontext;?></li>
        </ul>
      </div>
    </div>
  </a>
</div>

