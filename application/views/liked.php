<?php include("header-signup.php"); ?>
<body>
  <font face="Hiragino Maru Gothic Pro">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>
    
    <?php include("header-login.php") ?>
    
    <div class="container">

      <div class="row">
      </div>
      <div class="row">
        <div class="center">
          <div class="col s12 orange-text darken-4">
            <h4>いいねされた会社</h4>
          </div>
          <?php include("menu.php"); ?>
        </div>
      </div>
      
      <div class="row">
        
        <?php foreach($data as $test) { ?>
          <?php if(isset($test->presidentname)){ ?>
            <?php include("like_userlists.php") ?>
          <?php }else{;?>
            <?php echo "いいねされたユーザーはまだいません。";?>
          <?php }?>
        <?php }?>
        
      </div><!-- row -->
    </div><!-- container -->
  </font>
</body>
        </html>
