<?php include("header-signup.php") ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>

<div class="container">

  <div class="row">
        </div>
        <div class="row">
          <div class="container center">
            <div class="col s12 orange-text darken-4">
              <h4>ログイン画面</h4>
            </div>
            <?php
            $attributes = array('class' => 'col s12');
            echo form_open("main/login_validation",$attributes);
            //echo validation_errors();
            ?>
            <div class="row">
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <?php
                        $email = array(
                            'name'        => 'email',
                            'value'          => $this->input->post('email'),
                            'class'       => 'validate',
                            'id' => 'email'
                        );
                        echo form_input($email);
                        ?>
                            <label for="email">メールアドレス</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <?php
                        $password = array(
                            'name'        => 'password',
                            'value'          => $this->input->post('password'),
                            'class'       => 'validate',
                            'id' => 'password'
                        );
                        echo form_password($password);
                        ?>
                        <label for="password">パスワード</label>
                      </div>
                    </div>
                    <?php
                    echo form_submit("login_submit", "ログイン","class='waves-effect waves-light btn-large orange'");
                    echo form_close();
                    ?>
          </div>
        </div>
        
</font>
</body>
</html>
