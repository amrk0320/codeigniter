<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<div class="wrap top">
    <div class="in clearfix">
        <?php   if(isset($data) && count($data) > 0):?>
        <?php foreach($data as $test) { ?>
        <a href="<?php
        echo site_url('main/userDetailForAdmin/'.$test->id) ?>" class="hover">
            <table class="users">
                <tbody>
                <tr>
                    <th>会社名</div></th>
    <td><?php echo $test->company;?></td>
    </tr>
    <tr>
        <th>代表者名</th>
        <td>
            <?php echo $test->presidentname;?>
        </td>
    </tr>
    <tr>
        <th>設立年月日</th>
        <td>
            <?php   if(strlen($test->birthdt) > 1):?>
                <?php echo substr($test->birthdt, 0, 4)?>年<?php echo substr($test->birthdt, 4, 2)?>月<?php echo substr($test->birthdt, 6, 2)?>日
            <?php else: ?>
                設立年月日が未登録です。
            <?php endif;?>
        </td>
    </tr>
    <tr>
        <th>資本金</th>
        <td>
            <?php echo $test->capitalstock;?>円
        </td>
    </tr>
    <tr>
        <th>目標金額</th>
        <td>
            <?php echo $test->targetamount;?>円
        </td>
    </tr>
    <tr>
        <th>目的</th>
        <td>
            <?php echo $test->targetcontext;?>
        </td>
    </tr>
    <tr>
        <th>写真</th>
        <td>
            <?php if($test->pics != ""):?>
                <img src="<?php echo "http://localhost:8888/webApp/uploads/".$test->pics; ?>" width="200px" height="200px">
            <?php else: ?>
                画像がアップロードされていません。
            <?php endif;?>

        </td>
    </tr>
    </tbody>
    </table>
    </a>
    <?php }?>
        <?php else: ?>
            ユーザーが登録されていません。
        <?php endif;?>
    <a href="<?php echo site_url('main/admin') ?>">ログアウトする</a>
</div>
</div>
<footer>
    Copyright 会社を創ろう.com 2017 all rights reserved.
</footer>
</body>
</html>
