<div class="row show-on-small hide-on-med-and-up">
  <div class="col s12 row"><a href="<?php echo site_url(); ?>main/userlist" style="width: 300px" class="waves-effect waves-light orange btn">会社一覧</a></div>
  <div class="col s12 row"><a href="<?php echo site_url(); ?>main/like" style="width: 300px" class="waves-effect waves-light orange btn">いいねした会社</a></div>
  <div class="col s12"><a href="<?php echo site_url(); ?>main/liked" style="width: 300px" class="waves-effect waves-light orange btn">いいねされた会社</a></div>
</div>

<div class="row show-on-medium-and-up hide-on-small-only">
  <div class="row">
    <a href="<?php echo site_url(); ?>main/userlist" style="width: 200px" class="waves-effect waves-light orange btn">会社一覧</a>
    <a href="<?php echo site_url(); ?>main/like" style="width: 200px" class="waves-effect waves-light orange btn">いいねした会社</a>
    <a href="<?php echo site_url(); ?>main/liked" style="width: 200px" class="waves-effect waves-light orange btn">いいねされた会社</a>
  </div>
</div>
