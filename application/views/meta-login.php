<body>
  <font face="Hiragino Maru Gothic Pro">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>js/init.js"></script>
