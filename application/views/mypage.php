<?php include("header-signup.php"); ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
 //折れ線グラフ
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 function drawChart() {
     var data = google.visualization.arrayToDataTable([
         ['月', '売上'],
         ['1月',  <?php if(isset($january)){ echo $january; } else { echo 0; } ?>],
         ['2月',  <?php if(isset($february)){ echo $february; } else { echo 0; } ?>],
         ['3月',  <?php if(isset($march)){ echo $march; } else { echo 0; } ?>],
         ['4月',  <?php if(isset($april)){ echo $april; } else { echo 0; } ?>],
         ['5月',  <?php if(isset($may)){ echo $may; } else { echo 0; } ?>],
         ['6月',  <?php if(isset($june)){ echo $june; } else { echo 0; } ?>],
         ['7月',  <?php if(isset($july)){ echo $july; } else { echo 0; } ?>],
         ['8月',  <?php if(isset($august)){ echo $august; } else { echo 0; } ?>],
         ['9月',  <?php if(isset($september)){ echo $september; } else { echo 0; } ?>],
         ['10月',  <?php if(isset($october)){ echo $october; } else { echo 0; } ?>],
         ['11月',  <?php if(isset($november)){ echo $november; } else { echo 0; } ?>],
         ['12月',  <?php if(isset($december)){ echo $december; } else { echo 0; } ?>],
     ]);
     var options = {
         title: '会社業績'
     };
     var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
     chart.draw(data, options);
 }
</script>
<script type="text/javascript">
 google.load("visualization", "1", {packages:["corechart"]});
 google.setOnLoadCallback(drawChart);
 //円グラフ
 function drawChart() {
     var data = google.visualization.arrayToDataTable([ 
         //グラフデータの指定
         <?php if($stock == 0 && $business == 0 && $exchange == 0 && $investment == 0 ){ 
         ?>
         ['Task', '運用資金の状況'],
         ['登録なし',     1],
         <?php 
        } 
        else
        { 
        ?>
        ['Task', '運用資金の状況'],
        ['株',     <?php echo $stock; ?>],
        ['投信',    <?php  echo $investment; ?>],
        ['為替',  <?php echo $exchange; ?>],
        ['事業等', <?php echo $business; ?>],
        <?php 
        } 
       ?>
    ]);
    var options = { //オプションの指定
        pieSliceText: 'label',
        title: '運用資金の状況'
    };
    var chart = new google.visualization.PieChart(document.getElementById('piechart')); //グラフを表示させる要素の指定
    chart.draw(data, options);
}
</script>
<div class="container">

  <!-- button_edit -->
  <div class="fixed-action-btn horizontal">
    <?php
    echo form_open("main/edit", "class='btn-floating btn-large red'")
    ?>
    <?php
        echo form_submit("mode","編集","class='btn-floating btn-large red'");
    ?>
    <!--<i class="large material-icons">mode_edit</i>-->
    <?php
    
    $data=array(
        "company"=> $company,
        "presidentname"=> $presidentname,
        "birthdt"=> $birthdt,
        "capitalstock"=> $capitalstock,
        "targetamount"=> $targetamount,
        "targetcontext"=> $targetcontext,
        "filename"=> $filename
    );
    echo form_hidden($data);
    ?>
  </div>
  <!-- button_edit -->
  
  <div class="row">
  </div>
  <div class="row">
    <div class="center">
      <div class="col s12 orange-text darken-4">
        <h4><?php echo $_SESSION['presidentname']."さん";?>の会社情報</h4>
      </div>
      <?php include("menu.php"); ?>
    </div>
  </div>

  <?php if($company):{ ?>
  <div class="row">
    <div class="section center">
      <h5><?php echo $company;?></h5>
    </div>
  </div>
  <?php };endif; ?>
  
  <div class="row show-on-small hide-on-med-and-up">
    <div class="row">
      <div class="col s12">
        <div id="chart_div" style="width:90%;"></div>
        <div id="piechart" style="width:90%;"></div>
      </div>
      <div class="col s12">
        <div class="section">
          <?php if($filename != ""):?>
            <img class="responsive-img" src="<?php echo site_url();?>uploads/<?php echo $filename;?>">
          <?php else: ?>
            No Image.
          <?php endif;?>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>代表者取締役社長　<?php echo $presidentname?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>設立日 <?php   if(strlen($birthdt) > 1):?>
            <?php echo substr($birthdt, 0, 4)?>年<?php echo substr($birthdt, 4, 2)?>月<?php echo substr($birthdt, 6, 2)?>日
          <?php else: ?>
            設立日が未登録です。
          <?php endif;?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>資本金 <?php echo $capitalstock?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>目標金額 <?php echo $targetamount?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>概要 <?php echo $targetcontext?></h6>
        </div>
        <div class="divider"></div>
      </div>
    </div>
  </div>

  
  <div class="card">
    <div class="row show-on-medium-and-up hide-on-small-only">
      <div class="col s6">
        <div id="chart_div" style=""></div>
        <div id="piechart" style=""></div>
      </div>
      <div class="col s6">
        <div class="section">
          <?php if($filename != ""):?>
            <img class="responsive-img" src="<?php echo site_url();?>uploads/<?php echo $filename;?>">
          <?php else: ?>
            No Image.
          <?php endif;?>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>代表者取締役社長　<?php echo $presidentname?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>設立日 <?php   if(strlen($birthdt) > 1):?>
            <?php echo substr($birthdt, 0, 4)?>年<?php echo substr($birthdt, 4, 2)?>月<?php echo substr($birthdt, 6, 2)?>日
          <?php else: ?>
            設立年月日が未登録です。
          <?php endif;?></h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>資本金 <?php echo $capitalstock?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>目標金額 <?php echo $targetamount?>円</h6>
        </div>
        <div class="divider"></div>
        <div class="section">
          <h6>概要 <?php echo $targetcontext?></h6>
        </div>
        <div class="divider"></div>
      </div>
    </div>
    </font>
</body>
</html>
