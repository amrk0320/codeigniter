<?php include("header-signup.php") ?>
<?php include("meta-login.php") ?>
<?php include("header-login.php") ?>

<div class="container">
  
  <div class="row">
  </div>
  <div class="row">
    <div class="container center">
      <div class="col s12 orange-text darken-4">
        <h4>会員登録</h4>
        <font color="red">※すべて必須事項です。</font>
      </div>
      <?php
      $attributes = array('class' => 'col s12');
      echo form_open("main/signup_validation", $attributes);
      ?>
      <div class="row">
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $presidentname = array(
              'name'        => 'presidentname',
              'value'       => $this->input->post('presidentname'),
              'class'       => 'validate',
              'id'          => 'first_name',
          );
          echo form_input($presidentname);
          ?>
          <label for="first_name">お名前</label>
        </div>
        <?php echo form_error('presidentname', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $presidentnamekana = array(
              'name'        => 'presidentnamekana',
              'value'          => $this->input->post('presidentnamekana'),
              'class'       => 'validate',
              'id' => 'first_name',
          );
          echo form_input($presidentnamekana);
          ?>
          <label for="first_name">フリガナ</label>
        </div>
        <?php echo form_error('presidentnamekana', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $email = array(
              'name'        => 'email',
              'value'          => $this->input->post('email'),
              'class'       => 'validate',
              'id' => 'email'
          );
          echo form_input($email);
          ?>
          <label for="email">メールアドレス</label>
        </div>
        <?php echo form_error('email', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $cemail = array(
              'name'        => 'cemail',
              'value'          => $this->input->post('cemail'),
              'class'       => 'validate',
              'id' => 'cemail'
          );
          echo form_input($cemail);
          ?>
          <label for="cemail">メールアドレス確認用</label>
        </div>
        <?php echo form_error('cemail', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $password = array(
              'name'        => 'password',
              'class'       => 'validate',
              'id' => 'password',
          );
          echo form_password($password); //パスワードの入力フィールドを出力
          ?>
          <label for="password">パスワード</label>
        </div>
        <?php echo form_error('password', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <?php
          $cpassword = array(
              'name'        => 'cpassword',
              'class'       => 'validate',
              'id' => 'password',
          );
          echo form_password($cpassword);  //パスワードの入力フィールドを出力
          ?>
          <label for="password">パスワード確認用</label>
        </div>
        <?php echo form_error('cpassword', '<font color="red">', '</font>');?>
      </div>
      <div class="row">
        <a href="<?php echo site_url(); ?>main/login">会員登録している方はこちらからログイン</a>
      </div>
      <?php
      echo form_submit("signup_submit", "確認","class='waves-effect waves-light btn-large orange'");  //ユーザー登録ボタンを出力
      echo form_close();  //フォームを閉じる
      ?>
    </div>
  </div>
  
</div><!-- container -->
        </font>
</body>
            </html>
