<?php include("meta.php"); ?>
<?php include("header.php"); ?>
<body class="blue lighten-5">
  <font face="Hiragino Maru Gothic Pro">
    <div id="index-banner" class="parallax-container">
      <div class="section no-pad-bot">
        <div class="container">
          <br><br>
          <h3 class="header center blue-text darken-4">自分の資産を増やすには?</h3>
          <div class="row center">
            <h5 class="header col s12 light black-text darken-4">銀行に預ける？ 株を買う？ 不動産投資？<br>
              自分で学んで育てて増やしましょう。</h5>

            <a href="<?php echo site_url(); ?>main/signup" id="download-button" class="btn-large waves-effect waves-light blue darken-4">会員登録</a>
          </div>
          <br><br>

        </div>
      </div>
      <div class="parallax"><img src="<?php echo site_url();?>img/background1.jpg" alt="Unsplashed background img 1"></div>
    </div>


    <div class="container blue lighten-5">
      <div class="section">

        <!--   Icon Section   -->
        <div class="row">
          <div class="col s12 m4 blue lighten-4">
            <div class="icon-block">
              <h2 class="center brown-text"><i class="material-icons">flash_on</i></h2>
              <h5 class="center">個人ページの所得</h5>

              <p class="light">・SNSは同じ目的（資産運用）をもったメンバーのみ参加してるんです<br>
                ・自社の経営状況はグラフにて一目瞭然なんです<br>
                ・年度末に一年間の経営（運用）の実績が決算としてレポート報告されるので会社の進捗具合をしっかりとチェックしましょう<br>
                ・運用目標をたててしっかりと考えたロジカルな運用をしていきましょう</p>
            </div>
          </div>

          <div class="col s12 m4 blue lighten-5">
            <div class="icon-block">
              <h2 class="center brown-text"><i class="material-icons">group</i></h2>
              <h5 class="center">特徴と機能</h5>

              <p class="light">・会員サイトで他社の経営状況がチェックできちゃう<br>
                ・利益が出ている会社がいたら交渉してM&Aしちゃおう。<br>
                ・会社を売却して現金化したいなら倒産を宣告して会社を買ってもらおう。<br>
                ・その他機能も追加予定なんです</p>
            </div>
          </div>

          <div class="col s12 m4 blue lighten-4">
            <div class="icon-block">
              <h2 class="center brown-text"><i class="material-icons">settings</i></h2>
              <h5 class="center">運用アドバイスは運用コンサルティング会社がサポート</h5>

              <p class="light">・運用額と目標額などから運用のプロから適切なアドバイスを受けそして学んでいけちゃいます<br>
                ・IFA資格をもった会社より海外の証券も購入できちゃう<br>
                ・経営（運用）ででた利益と学びをバーチャルからリアルの会社設立しよう もちろんリアルもサポートしてくれちゃいます</p>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="parallax-container valign-wrapper">
      <div class="section no-pad-bot">
        <div class="container">
          <div class="row center">
            <h5 class="header col s12 light black-text">会社を創ろう.comとは?</h5>
          </div>
        </div>
      </div>
      <div class="parallax"><img src="<?php echo site_url();?>img/background1.jpg" alt="Unsplashed background img 2"></div>
    </div>

    <div class="container">
      <div class="section">

        <div class="row">
          <div class="col s12 center">
            <h3><i class="mdi-content-send brown-text"></i></h3>
            <h5>会社を創ろう.comとは、</h5>
            <p class="left-align light">擬似会社設立型資産運用SNSです！<br>
              バーチャルの会社を設立して資産運用という業務をこなし利潤を追求していきましょう！<br>
              初めて資産運用する方にも分かりやすくサポートし、他社の経営（運用）状態を見ながら学んで自社の経営にも取りいれて行きましょう！<br>
              そして運用利益で自分へのご褒美や貯蓄や更なる投資、またはバーチャルから飛び出してリアルでも会社設立を目指してみるナンてはどうでしょうか？<br>
              自分の成長が大きな可能性に繋がるようサポートしていくツールです！</p>
            <div class="row">
            </div>
            <h5>会社ページの見方</h5>
          </div>
          <img class="responsive-img" src="<?php echo site_url();?>img/sample.png">
        </div>

      </div>
    </div>

    <div class="parallax-container valign-wrapper">
      <div class="section no-pad-bot">
        <div class="container">
        </div>
      </div>
      <div class="parallax"><img src="<?php echo site_url();?>img/background1.jpg" alt="Unsplashed background img 2"></div>
    </div>
    <?php include("footer.php"); ?>
