 $(function(){
   // #で始まるアンカーをクリックした場合に処理
     $(".scroll_btn a.scroll").click(function() {
      // スクロールの速度
      var speed = 1500; // ミリ秒
　　　 // 移動先を取得
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
     });

      // #で始まるアンカーをクリックした場合に処理
     $("#go_top a").click(function() {
      // スクロールの速度
      var speed = 1500; // ミリ秒
　　　 // 移動先を取得
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
  });

//$(function(){
//	$("a img").mouseover(function(){
//		$(this).fadeTo(500,0.8)
//	});
//	$("a img").mouseout(function(){
//		$(this).fadeTo(200,1.0)
//	});
//});

// page Topフェードイン・アウト
$(function(){
	  $(window).bind("scroll", function() {
	      // トップから150px以上スクロールしたら
	      if ($(this).scrollTop() > 1800) { 
	          // ページトップのリンクをフェードインする
		        $("#go_top").fadeIn();
	      } else { // それ以外は
	          // ページトップのリンクをフェードアウトする
		        $("#go_top").fadeOut();
	      }
    });
})

$(function(){
	  $(document).ajaxComplete(function(){
        $(".vl-inner").mouseover(function(){
		        $(this).fadeTo(400,0.6)
        });
	  });
    $(document).ajaxComplete(function(){
        $(".vl-inner").mouseout(function(){
		        $(this).fadeTo(200,1.0)
	      });
    });
});
